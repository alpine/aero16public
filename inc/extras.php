<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Aero Air
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function aero_air_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'aero_air_body_classes' );

function aero_air_excerpt_heading_levels( $translation, $original )
{
	if ( 'section' == get_post_type() ) {
		if ( 'Excerpt' == $original ) {
			return 'Section subheading';
		}else{
			$pos = strpos($original, 'Excerpts are optional hand-crafted summaries of your');
			if ($pos !== false) {
				return  'Sub heading to appear under section title';
			}
		}
	}

	return $translation;
}
add_filter( 'gettext', 'aero_air_excerpt_heading_levels', 10, 2 );


function aero_air_section_set( $atts ) {
	global $post;
	$a = shortcode_atts( array(
		'slug' => 'null',
		'bar' => 'something else',
	), $atts );

	$args = array(
		'posts_per_page'   => 0,
		'offset'           => 0,
		'orderby'          => 'date',
		'order'            => 'DESC',
		'post_type'        => 'section',
		//'post_mime_type'   => '',
		//'post_parent'      => '',
		//'author'	   => '',
		'post_status'      => 'publish',
		//'suppress_filters' => true
	);
	$sections = get_posts( $args );
	ob_start();

	foreach ( $sections as $post) {
		setup_postdata( $post );

		$template_base = 'template-parts/content';

		if ( locate_template($template_base . '-' . get_post_type() . '.php', false) ) {
			get_template_part( $template_base . '-' . get_post_type(), get_post_format() );
		} else {
			get_template_part( $template_base, get_post_format() );
		}
	}

	wp_reset_postdata();

	return ob_get_clean();
}
add_shortcode( 'sectionset', 'aero_air_section_set' );
