<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Aero Air
 */

?>

		</div>
	</div><!-- #content -->
</div>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<?php
		$second_menu = wp_nav_menu( array(
				'echo'            => false,
				'menu'            => 'social',
				'theme_location'  => 'social',
				'depth'           => 1,
				'container'       => null,
				'menu_class'      => 'nav navbar-nav pull-xs-right social-navigation',
				'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
				'walker'          => new wp_bootstrap_navwalker(),
			)
		);
		wp_nav_menu( array(
				'menu'            => 'footer',
				'menu_id'         => 'footer-menu',
				'theme_location'  => 'footer',
				'depth'           => 1,
				'container'       => 'nav',
				'container_class' => 'navbar navbar-dark bg-inverse footer-navigation',
				'container_id'    => 'footer-navigation',
				'menu_class'      => 'nav navbar-nav',
				'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
				'walker'          => new wp_bootstrap_navwalker(),
				'right_menu'      => $second_menu
			)
		);
		?>
		<div class="site-info">
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
