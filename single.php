<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Aero Air
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="tmain" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();
			$template_base = 'template-parts/content';
			if ( locate_template($template_base . '-' . get_post_type() . '.php', false) ) {
				get_template_part( $template_base . '-' . get_post_type(), get_post_format() );
			} else {
				get_template_part( $template_base, get_post_format() );
			}

			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
