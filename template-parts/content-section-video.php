<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Aero Air
 */

$alignment = get_post_meta( get_the_ID(), 'alignment', true);
?>
<section id="post-<?php the_ID(); ?>" <?php post_class($alignment); ?>>
	<video class="hidden-xs" preload="auto" autoplay="" loop="" muted="" id="bgvid" poster="">
		<source src="//d2edahx6g47lzn.cloudfront.net/ICON_WEB_REFRESH_VIDEO_CUT_4_1080_v01_webm.webm" type="video/webm">
		<source src="//d2edahx6g47lzn.cloudfront.net/ICON_WEB_REFRESH_VIDEO_CUT_4_1080_v01_p_q25.mp4" type="video/mp4">
	</video>
	<div class="entry-content">
		<?php
			the_title( '<h1 class="section-heading">', '</h1>' );
			echo sprintf('<h2>%s</h2>', get_the_excerpt() );
		?>
	</div><!-- .entry-content -->

</section><!-- #post-## -->
