<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Aero Air
 */

$classes = get_post_meta( get_the_ID(), 'alignment', true);
if ( get_post_meta( get_the_ID(), 'darken_menu', true) ) {
	$classes .= ' darken';

}
?>
<section id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>

	<div class="entry-content">
		<?php
			the_title( '<h1 class="section-heading">', '</h1>' );
			echo sprintf('<h2>%s</h2>', get_the_excerpt() );
		?>
	</div><!-- .entry-content -->

</section><!-- #post-## -->
