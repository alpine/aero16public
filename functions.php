<?php
/**
 * Aero Air functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Aero Air
 */

if ( ! function_exists( 'aero_air_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function aero_air_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Aero Air, use a find and replace
	 * to change 'aero_air' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'aero_air', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'aero_air' ),
		'primary-right' => esc_html__( 'Primary Right', 'aero_air' ),
		'footer' => esc_html__( 'Footer', 'aero_air' ),
		'social' => esc_html__( 'Social', 'aero_air' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		//'aside',
		'image',
		'video',
		//'quote',
		//'link',
	) );


	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'aero_air_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'aero_air_setup' );

function aero_air_post_type_setup() {
	remove_post_type_support( 'section', 'post-formats' );
	add_post_type_support( 'section', 'post-formats', array( 'aside', 'image', 'video' ) );
}
add_action( 'init', 'aero_air_post_type_setup', 2, 15);

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function aero_air_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'aero_air_content_width', 640 );
}
add_action( 'after_setup_theme', 'aero_air_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function aero_air_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'aero_air' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'aero_air_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function aero_air_scripts() {

	wp_enqueue_style( 'aero-air-style',  get_template_directory_uri() . '/assets/css/main.min.css' );

	wp_enqueue_style( 'google-fonts-open-sans', '//fonts.googleapis.com/css?family=Open+Sans:400,700,800,300' );

	wp_enqueue_script( 'aero-air-main', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), null, true );

	/*
	wp_enqueue_script( 'aero-air-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'aero-air-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	*/
}
add_action( 'wp_enqueue_scripts', 'aero_air_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Include bootstrap nav walker class
 */
require get_template_directory() . '/inc/wp_bootstrap_navwalker.php';
