<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Aero Air
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'aero_air' ); ?></a>

	<header id="masthead" class="site-header" role="banner">

		<?php
		$second_menu = wp_nav_menu( array(
				'echo'            => false,
				'menu'            => 'primary-right',
				'theme_location'  => 'primary-right',
				'depth'           => 2,
				'container'       => null,
				'menu_class'      => 'nav navbar-nav pull-xs-right',
				'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
				'walker'          => new wp_bootstrap_navwalker(),
			)
		);
		wp_nav_menu( array(
				'show_branding'   => true,
				'mobile_collapse' => true,
				'menu'            => 'primary',
				'menu_id'         => 'primary-menu',
				'theme_location'  => 'primary',
				'depth'           => 2,
				'container'       => 'nav',
				'container_class' => 'navbar navbar-fixed-top navbar-dark bg-inverse header-navigation navbar-full',
				'container_id'    => 'main-navigation',
				'menu_class'      => 'nav navbar-nav',
				'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
				'walker'          => new wp_bootstrap_navwalker(),
				'right_menu'      => $second_menu
			)
		);
		?>
	</header><!-- #masthead -->
	<div class="content-wrap">

	<div id="content" class="site-content">
		<div class="row">
